using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MySql.EntityFrameworkCore.Bug.Reappear.Data;

namespace MySql.EntityFrameworkCore.Bug.Reappear.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HealthCheckController : ControllerBase
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly ILogger<HealthCheckController> _logger;

        public HealthCheckController(IServiceScopeFactory serviceScopeFactory, ILogger<HealthCheckController> logger)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _logger = logger;
        }

        [HttpGet("HealthCheck")]
        [AllowAnonymous]
        public async Task<IActionResult> HealthCheck()
        {
            using var scope = _serviceScopeFactory.CreateScope();
            var dbContext = scope.ServiceProvider.GetRequiredService<BloggingContext>();
            try
            {
                var list = await dbContext.Blogs.ToListAsync();  // uncatchable exceptions,
                return Ok(new { Status = "ok", Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return Problem(detail: ex.Message);
            }
        }
    }
}
