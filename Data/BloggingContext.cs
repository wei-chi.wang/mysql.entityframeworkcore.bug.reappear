﻿using Microsoft.EntityFrameworkCore;

namespace MySql.EntityFrameworkCore.Bug.Reappear.Data
{
    public class BloggingContext : DbContext
    {
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }

        public BloggingContext()
        {
        }

        public BloggingContext(DbContextOptions<BloggingContext> options)
            : base(options)
        {
        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL(@"Server=111.111.111.111;Port=3306;Uid=user;Pwd=password;DataBase=blogging;Convert Zero Datetime=True;CharSet=utf8;");
        }
    }
}
